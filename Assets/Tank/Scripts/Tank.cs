﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tank : MonoBehaviour
{
    public float TankSpeed = 5;
    public float LookSpeed = 0.5f;
    public GameObject Turret;
    public GameObject MainBody;
    public GameObject Missile;
    public GameObject MissileSpawn;

    private readonly float MissileSpeed = 250000;
    private readonly float ShootDelay = 2;

    private bool CanShoot = true;

    public IEnumerator ShootMissile(GameObject missile)
    {
        CanShoot = false;
        GameObject MissileClone = Instantiate(missile, MissileSpawn.transform.position, Turret.transform.localRotation);
        MissileClone.GetComponent<Rigidbody>().AddRelativeForce(-Turret.transform.forward * MissileSpeed);
        Destroy(MissileClone, 10);
        yield return new WaitForSeconds(ShootDelay);
        CanShoot = true;
    }

    public void TurnTurret(Vector3 direction)
    {
        Turret.transform.Rotate(direction);
    }

    public void TurnMainBody(Vector3 direction)
    {
        MainBody.transform.Rotate(direction);
    }

    public void ResetRotation()
    {
        transform.rotation = Quaternion.identity;
    }

    public void Move(Vector3 direction)
    {
        GetComponent<Rigidbody>().velocity = direction;
    }

    private void Update()
    {
        TurnTurret(Vector3.forward * Input.GetAxisRaw("Horizontal") * LookSpeed);

        if (Input.GetKey("q"))
        {
            TurnMainBody(-Vector3.forward);
        }
        else if (Input.GetKey("e"))
        {
            TurnMainBody(Vector3.forward);
        }
        else if (Input.GetKey("f") && !MovingVertically())
        {
            ResetRotation();
        }
        else if (Input.GetMouseButtonDown(0) && CanShoot)
        {
            StartCoroutine(ShootMissile(Missile));
        }
    }

    private void FixedUpdate()
    {
        if (!MovingVertically())
        {
            Move(-MainBody.transform.up * Input.GetAxisRaw("Vertical") * TankSpeed + Vector3.up * GetComponent<Rigidbody>().velocity.y);
        }
    }

    private bool MovingVertically()
    {
        return Math.Abs(GetComponent<Rigidbody>().velocity.y) > 0.2f;
    }
}